package uz.pdp.db;

import lombok.SneakyThrows;
import uz.pdp.model.Book;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static uz.pdp.config.database.*;

public class Db_Service {


    @SneakyThrows
    public static Connection getConnection() {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(db_url, db_username, db_password);
    }


    public static List<Book> getAllBooks() throws SQLException {
        List<Book> bookList = new ArrayList<>();
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "select * from book";
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            bookList.add(
                    new Book(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3))
            );
        }
        return bookList;
    }

    @SneakyThrows
    public static void addBook(String title, String author) {
        Connection connection = getConnection();
        String query="insert into book (title,author) values (?,?);";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1,title);
        preparedStatement.setString(2,author);
        preparedStatement.execute();
    }

    @SneakyThrows
    public static Book getBookByIdFromDb(Integer id) {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query="select * from book where id="+id;
        ResultSet resultSet = statement.executeQuery(query);
        Book book = new Book();
        while (resultSet.next()){
            book.setId(resultSet.getInt(1));
            book.setTitle(resultSet.getString(2));
            book.setAuthor(resultSet.getString(3));
        }
        return book;
    }

    @SneakyThrows
    public static void updatedBook(Book book) {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query="update book set title='"+
                book.getTitle()+"',author='"+
                book.getAuthor()+"' where id="+book.getId()+";";
        statement.execute(query);
    }

    public static void deleteStudentByIdFromDb(Integer id) throws SQLException {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query="delete from book where id="+id;
        statement.execute(query);
    }


    @SneakyThrows
    public static boolean isAdmin(String username, String password){
        Class.forName("org.postgresql.Driver");
        Connection connection = getConnection();
        String query="select a.is_admins from admins a where a.username=? and a.password = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1,username);
        preparedStatement.setString(2,password);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            boolean isAdmin1 = resultSet.getBoolean(1);
            if (isAdmin1) {
                return true;
            }
        }
        return false;
    }
}

