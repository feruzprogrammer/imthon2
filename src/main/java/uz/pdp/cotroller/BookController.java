package uz.pdp.cotroller;

import lombok.SneakyThrows;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.db.Db_Service;
import uz.pdp.model.Book;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.List;

@Controller
public class BookController {
    @GetMapping(value ="/login")
    public String homepage (){
        return "index";
    }

    @PostMapping(value = "/login")
    public String singIn(HttpServletRequest request) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        boolean isAdmin = Db_Service.isAdmin(username, password);
        if (!isAdmin){
            return "adminPage";
        }
        else return "userPage";
    }


    @SneakyThrows
    @GetMapping("/allBooks")
    public String allBooks(Model model) {
        Class.forName("org.postgresql.Driver");
        List<Book> bookList = Db_Service.getAllBooks();
        model.addAttribute("barchaKitoblar", bookList);
        return "allBooks";
    }
    @GetMapping("/add")
    public String getCreateStuntPage(){
        return "createBook";
    }

    @SneakyThrows
    @PostMapping(value = "/add")
    public String saveBook(HttpServletRequest request, Model model) {
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        Db_Service.addBook(title,author);
        List<Book> allBookList = Db_Service.getAllBooks();
        model.addAttribute("barchaKitoblar",allBookList);
        return "allBooks";
    }

    @GetMapping("/edit")
    public String editStudent(@RequestParam("id") Integer id, Model model) throws SQLException {
        Book book = Db_Service.getBookByIdFromDb(id);
        model.addAttribute("updateBook", book);
        return "update";
    }

    @PostMapping("/edit")
    public String updatedStudent(@RequestParam Integer id,
                                 @RequestParam String title,
                                 @RequestParam String author,
                                 Model model) throws SQLException, ClassNotFoundException {
        Db_Service.updatedBook(new Book(id, title, author));
        Class.forName("org.postgresql.Driver");
        List<Book> bookList = Db_Service.getAllBooks();
        model.addAttribute("barchaKitoblar", bookList);
        return "allBooks";
    }

    @GetMapping("/delete")
    public String deleteStudent(@RequestParam("id") Integer id, Model model) throws SQLException, ClassNotFoundException {
        Db_Service.deleteStudentByIdFromDb(id);
        Class.forName("org.postgresql.Driver");
        List<Book> bookList = Db_Service.getAllBooks();
        model.addAttribute("barchaKitoblar", bookList);
        return "allBooks";
    }
}
